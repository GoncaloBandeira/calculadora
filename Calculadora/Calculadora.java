package Calculadora;

import java.lang.String;
import java.lang.System;
import java.util.InputMismatchException;
import java.util.Objects;
import java.util.Scanner;

public class Calculadora {


    /**
     * Resolves User input and calls operations.
     */

    public static double retMult(double x, double y){
        return  x * y;
    }

    public static double retSum(double x, double y){
        return x + y;
    }

    public static double retSub(double x, double y){
        return  x - y;
    }

    public static double retDiv(double x, double y){
        return  x / y;
    }

    public static double retcilVol(double r, double h){
        return  Math.PI*(r*r)*h;
    }

    public static double retRaised(double n1,double n2){
        return Math.pow(n1,n2);
    }

    public static String retBin(double n1){
        if (n1 == (int) n1) {
            return Integer.toBinaryString((int) n1);
        } else {
            int parteInteira = (int) n1;
            double parteFracionaria = n1 - parteInteira;

            StringBuilder parteFracionariaBinaria = new StringBuilder();
            int casasDecimais = 0;
            while (parteFracionaria != 0 && casasDecimais < 5) { // Limita a 16 casas decimais
                parteFracionaria *= 2;
                int bit = (int) parteFracionaria;
                parteFracionariaBinaria.append(bit);
                parteFracionaria -= bit;
                casasDecimais++;
            }

            return Integer.toBinaryString(parteInteira) + "." + parteFracionariaBinaria.toString();
        }
    }

    public static double retSqrt(double n1){

        if(n1 < 0){
            return -1;
        }
        else{
            return Math.sqrt(n1);
        }
    }

    public static void terminal(){
        Scanner kb = new Scanner(System.in);

        double n1,n2;
        String opt;

        while(true) {
            System.out.println("Choose a Operation to realize: + - * / ^ cilVol binary (exit to exit)");
            opt = kb.nextLine();
            n2=0;
            n1=0;
            switch (opt) {
                case "+","-","/","*","^"->{
                    try {
                        System.out.println("Enter the numbers involved (seperated by a space or enter) or dot (.)/letter to Exit: ");
                        n1 = kb.nextDouble();
                        n2 = kb.nextDouble();
                    } catch (InputMismatchException e) {
                        System.out.println("Exit call/Error: Number not recognized. " +
                                "\nTip: do not use dots (.) to represent decimals. " +
                                "Use commas (,) instead (Exception: InputMismatchException).");
                        break;
                    }
                }
                case "cilVol"-> {
                    System.out.println("Enter the radius and height involved (seperated by a space or enter) or dot (.)/letter to Exit: ");
                    n1 = kb.nextDouble();
                    n2 = kb.nextDouble();
                }
                case "binary"-> {
                    System.out.println("Enter the number you wish to transform to binary/letter to Exit: ");
                    n1 = kb.nextDouble();
                }
                case "sqrt" ->{
                    System.out.println("Enter the number you wish to square root to Exit: ");
                    n1 = kb.nextDouble();
                }
                default -> System.out.println("Operation not recognized. Choose + - * / ^ cilVol binary or write 'exit' to exit.");
            }

            kb.nextLine();


            switch (opt) {
                case "+" -> {
                    //sum operation = new sum(n1, n2);
                    //System.out.println("Result: "+operation.getResult());
                    System.out.println("Result: "+retSum(n1,n2));
                }
                case "*" -> {
                    //mul operation = new mul(n1, n2);
                    //System.out.println("Result: "+operation.getMul());
                    System.out.println("Result: "+retMult(n1,n2));
                }
                case "-" -> {
                    //subtraction operation = new subtraction();
                    //System.out.println("Result: "+ operation.calc(n1, n2));
                    System.out.println("Result: "+retSub(n1,n2));
                }
                case "/" ->{
                    //div operation = new div(n1,n2);
                    //System.out.println("Result: "+ operation.getDiv());
                    System.out.println("Result: "+retDiv(n1,n2));
                }
                case "^" ->{
                    //Raised operation = new Raised();
                    //System.out.println("Result: "+ operation.calc(n1, n2));
                    System.out.println("Result: "+retRaised(n1,n2));
                }
                case "cilVol" ->{
                    //Raised operation = new Raised();
                    //System.out.println("Result: "+ operation.calc(n1, n2));
                    System.out.println("Result: "+retcilVol(n1,n2));
                }
                case "binary" ->{
                    System.out.println("Result: "+retBin(n1));
                }
                case "sqrt" ->{
                    if(retSqrt(n1) == -1)
                        System.out.println("The value you entered cannot be used in this operation. Try a number above 0.");
                        else
                        System.out.println("Result: "+retSqrt(n1));
                }
                //CALL YOUR SUBTRACTION CLASS HERE.

                case "exit" -> {
                    return;
                }
                default -> System.out.println("Operation not recognized. Choose + - * / ^ cilVol binary or write 'exit' to exit.");
            }
        }
    }

    public static void main(String[] args){
        System.out.println("[Test/Debug]: Hello World.");

        terminal();


        System.out.println("\033[3mPress [Enter] to exit...\033[0m");
        Scanner kb = new Scanner(System.in);

        String enter = kb.nextLine();
    }

    public Object returnNull() {
        return null;
    }
}
