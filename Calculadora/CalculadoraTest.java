package Calculadora;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class CalculadoraTest {
    Calculadora calc;
    @BeforeEach
    void setupBefEach(){
        calc = new Calculadora();
    }
    @DisplayName("Multiplication method test")
    @RepeatedTest(3)
    @Order(1)
    @Test
    void retMult() {
        assertEquals(4,calc.retMult(2,2));
        assertEquals(9,calc.retMult(3,3));
    }

    @Test
    void retSum() {
        assertEquals(2, calc.retSum(1,1));
        assertEquals(3, calc.retSum(2,1));
        assertEquals(4, calc.retSum(1,3));
        assertNotEquals(9,calc.retSum(1,1));
        assertNotEquals(1,calc.retSum(2,2));
        assertTrue(calc.retSum(1,1) == 2);
        assertFalse(calc.retSum(2,1) == 6);
    }

    @Test
    void retSub() {

    }

    @Test
    void retDiv() {
        assertEquals(2, calc.retDiv(2,2));
        assertEquals(4, calc.retDiv(8,2));
        assertEquals(3, calc.retDiv(6,2));
        assertNotEquals(9,calc.retDiv(16,2));
        assertNotEquals(1,calc.retDiv(2,1));
        assertTrue(calc.retDiv(2,2) == 1);
        assertFalse(calc.retDiv(2,3) == 1);
    }

    @Test
    void retcilVol() {
    }

    @Test
    void retRaised() {
        assertEquals(4,calc.retRaised(2,2));
        assertEquals(9,calc.retRaised(3,2));
        assertNotEquals(10,calc.retRaised(2,2));
        assertTrue(calc.retRaised(2,2) == 4);
        assertFalse(calc.retRaised(2,2) == 5);
        assertNotNull(calc.retRaised(2,2));
        assertNull(calc.returnNull());
    }

    @Test
    void retBin() {
        // Números inteiros
        assertEquals("10", calc.retBin(2));
        assertEquals("101", calc.retBin(5));
        assertEquals("11100", calc.retBin(28));
        // Número decimal
        assertEquals("10.1", calc.retBin(2.5));
        assertEquals("101.01", calc.retBin(5.25));
        assertEquals("11100.001", calc.retBin(28.125));
    }

    @Test
    void retSqrt() {

        assertEquals(2, calc.retSqrt(4));
        assertEquals(3, calc.retSqrt(9));
        assertEquals(4, calc.retSqrt(16));
        assertNotEquals(1,calc.retSqrt(2));
        assertNotEquals(1.5,calc.retSqrt(3));
        assertNotEquals(3,calc.retSqrt(6));
    }
}