package Calculadora;

public class sum {
    private double num1, num2, result=0;

    /**
     * Creates a sum operation (n1+n2). To get result use 'sumObj.getResult()'.
     *<p></p>
     *<p>----Example----
     *<p>  double n1=1;</p>
     *<p>  double n2=2;</p>
     *<p>  sum sumObj = new sum(n1,n2);</p>
     *<p>  double res = sumObj.getResult;</p>
     *<p>  //res=3</p>
     *
     * @param n1 number 1 (double)
     * @param n2 number 2 (double)
     */
    public sum(double n1, double n2) {
        num1 = n1;
        num2 = n2;
        result = num1 + num2;
    }

    /**
     * Returns the resulting operation realized on the object sum created.
     * @return result of n1+n2
     */
    public double getResult(){
        return result;
    }

}
